import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ElectricityBillTest {

    /*
    * Sample Input 1
    * 100 hours of fan
    * 720 hours of refrigerator
    * 80 hours of TV
    * 20 hours of grinder
    * */
    @Test
    void shouldPayZeroForSampleInputOne() {
        DomesticBill bill = new DomesticBill();
        assertEquals(0f, bill.calculate(100, 720, 80, 20, 0), 0.01f);
    }

    /*
     * Sample Input 2
     * 1000 hours of fan
     * 7200 hours of refrigerator
     * 800 hours of TV
     * 200 hours of grinder
     * */
    @Test
    void shouldPayAroundThreeKForSampleInputTwo() {
        DomesticBill bill = new DomesticBill();
        assertEquals(2891.6f, bill.calculate(1000, 7200, 800, 200, 0), 0.01f);
    }

    @Test
    void shouldPay1100ForCommercialBill() {
        CommercialBill bill = new CommercialBill();
        assertEquals(4200.0f, bill.calculate(3000, 0, 0, 300, 400), 0.01f);
    }
}