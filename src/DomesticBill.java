public class DomesticBill extends ElectricityBill {
    private float[] slabRatesUpto200Units;
    private float[] slabRatesUpto500Units;
    private float[] slabRatesAbove500Units;

    protected void setSlabRates() {
        slabRatesUpto200Units = new float[2];
        slabRatesUpto200Units[0] = 0f;
        slabRatesUpto200Units[1] = 1.5f;


        slabRatesUpto500Units = new float[3];
        slabRatesUpto500Units[0] = 0f;
        slabRatesUpto500Units[1] = 2f;
        slabRatesUpto500Units[2] = 3f;


        slabRatesAbove500Units = new float[4];
        slabRatesAbove500Units[0] = 0f;
        slabRatesAbove500Units[1] = 3.5f;
        slabRatesAbove500Units[2] = 4.6f;
        slabRatesAbove500Units[3] = 6.6f;
    }

    protected int[] getDifferences() {
        return new int[]{100, 100, 300};
    }

    protected float[] slabRatesFor(float units) {
        float[] slabRates;

        if (units < 201) {
            slabRates = slabRatesUpto200Units;
        } else if (units < 501) {
            slabRates = slabRatesUpto500Units;
        } else {
            slabRates = slabRatesAbove500Units;
        }

        return slabRates;
    }
}
