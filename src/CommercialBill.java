public class CommercialBill extends ElectricityBill {
    private float[] slabRates;

    protected void setSlabRates() {
        slabRates = new float[2];
        slabRates[0] = 0f;
        slabRates[1] = 7f;
    }

    protected int[] getDifferences() {
        return new int[]{500};
    }

    protected float[] slabRatesFor(float units) {
        return slabRates;
    }
}
