import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ElectricityBill {

    HashMap<String, Float> energyConsumption;

    public ElectricityBill() {
        energyConsumption = new HashMap<>();
        energyConsumption.put("fan", 0.06f);
        energyConsumption.put("refrigerator", 0.07f);
        energyConsumption.put("tv", 0.04f);
        energyConsumption.put("grinder", 0.40f);
        energyConsumption.put("waterHeater", 2.00f);

        setSlabRates();
    }

    protected void setSlabRates() {
        //define in derived class
    }

    public float calculate(float fan, float refrigerator, float tv, float grinder, float waterHeater) {
        float units = 0;
        units += fan * energyConsumption.get("fan");
        units += refrigerator * energyConsumption.get("refrigerator");
        units += tv * energyConsumption.get("tv");
        units += grinder * energyConsumption.get("grinder");
        units += waterHeater * energyConsumption.get("waterHeater");

//        System.out.println(units);

        List<Float> unitBreakdown = getUnitBreakDown(units);

        float total = 0f;
        int counter = 0;

        float[] slabToUse = slabRatesFor(units);

//        System.out.println(slabToUse.length);
//        System.out.println(unitBreakdown.size());

        for (float breakdown : unitBreakdown) {
            System.out.println("breakdown: " + breakdown + " slabval: " + slabToUse[counter]);
            total += breakdown * slabToUse[counter];
            counter++;
            if (counter == slabToUse.length)
                break;
        }

        return total;
    }

    private List<Float> getUnitBreakDown(float units) {
        List<Float> unitBreakdown = new ArrayList<Float>();
        int[] differences = getDifferences();
        int counter = 0;
        while (units > 0 && counter < differences.length) {
            float breakdown = 0;
            if (units >= differences[counter]) {
                breakdown = differences[counter];
                units -= differences[counter];
            } else {
                breakdown = units;
                units = 0;
            }
            unitBreakdown.add(breakdown);
            counter++;
        }

        if (units > 0)
            unitBreakdown.add(units);

        return unitBreakdown;
    }

    protected int[] getDifferences() {
        return new int[0];
    }

    protected float[] slabRatesFor(float units) {
        //define in derived class
        return new float[0];
    }
}
